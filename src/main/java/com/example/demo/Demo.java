package com.example.demo;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Demo
{
    
    @Autowired
    RedisRepository redisRepository;
    
    public void testRedis() {
        Inner inner = new Inner();
        inner.setIbans(Arrays.asList("12345", "45678"));
        
        Outer outer = new Outer();
        outer.setInners(Arrays.asList(inner));
        
        redisRepository.save(outer);
        System.out.println("Saved entity (correct): " + outer.toString());
        
        Outer redisOuter = redisRepository.findById(outer.getId()).get();
        System.out.println("Loaded entity (not correct): " + redisOuter.toString());
    }
}

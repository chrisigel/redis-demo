package com.example.demo;

import java.util.List;

import org.springframework.data.redis.core.RedisHash;

@RedisHash
public class Outer
{
    private String id;
    private List<String> ibans;
    private List<Inner> inners;

    public Outer()
    {
        super();
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public List<String> getIbans()
    {
        return ibans;
    }

    public void setIbans(List<String> ibans)
    {
        this.ibans = ibans;
    }

    public List<Inner> getInners()
    {
        return inners;
    }

    public void setInners(List<Inner> inners)
    {
        this.inners = inners;
    }

    @Override
    public String toString()
    {
        return "Outer [id=" + id + ", ibans=" + ibans + ", inners=" + inners + "]";
    }
    
}

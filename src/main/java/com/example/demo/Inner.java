package com.example.demo;

import java.util.List;

public class Inner
{
    private List<String> ibans;

    public Inner()
    {
        super();
    }

    public List<String> getIbans()
    {
        return ibans;
    }

    public void setIbans(List<String> ibans)
    {
        this.ibans = ibans;
    }

    @Override
    public String toString()
    {
        return "Inner [ibans=" + ibans + "]";
    }
    
}
